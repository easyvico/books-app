import React, { Component } from 'react';
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Input, Label } from 'reactstrap';
import axios from 'axios';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			books: [],

			newBookData: {
				title: '',
				rating: ''
			},
			editBookData: {
				id: '',
				title: '',
				rating: ''
			},

			userInput: '',
			newBookModal: false,
			editBookModal: false
		};

		this.toggleNewBookModal = this.toggleNewBookModal.bind(this);
		this.toggleEditBookModal = this.toggleEditBookModal.bind(this);
		this.handleUserInputTitle = this.handleUserInputTitle.bind(this);
		this.handleUserInputRating = this.handleUserInputRating.bind(this);
		this.editUserInputTitle = this.editUserInputTitle.bind(this);
		this.editUserInputRating = this.editUserInputRating.bind(this);
		this.addBook = this.addBook.bind(this);
		this.updateBook = this.updateBook.bind(this);
		//this.editBook = this.editBook.bind(this, book.id, book.title, book.rating);
	}

	// Appels vers le serveur
	componentWillMount() {
		this._refreshBooks();
	}

	//la methode envoie les données aux serveur qui renvoie une réponse (response.data) 
	addBook() {
		axios.post('http://localhost:3000/bookData', this.state.newBookData).then((response) => {

			let { books } = this.state;
			//on pousse les nouvelles données reçues dans notre tableau "books[]"
			books.push(response.data);

			// maj des donnees de books, on ferme la modal et on vide title, rating pour le prochain ajout
			this.setState({
				books,
				newBookModal: false,
				newBookData: {
					title: '',
					rating: ''
				}
			});
		});
	}

	// Modal comportement
	toggleNewBookModal() {
		this.setState({
			newBookModal: ! this.state.newBookModal
		});
	}
	toggleEditBookModal() {
		this.setState({
			editBookModal: ! this.state.editBookModal
		});
	}

	updateBook() {
		let { title, rating, id } = this.state.editBookData;
		//on va chercher le bon livre avec son id, on envoie le titre et rating a updater
		axios.put('http://localhost:3000/bookData/' + id, { title, rating })
		.then((response) => {
			// on rafraichit les données avec le GET de _refreshbooks...
			this._refreshBooks();

			// ...on met a jour en fermant la modal et en mettant des string vide
			this.setState({
				editBookModal: false,
				editBookData: {
					id: '',
					title: '',
					rating: ''
				}
			})
		});
	}

	editBook(id, title, rating) {
		this.setState({
			// envoie les bonnes infos a la modal editBookModal
			editBookData: { id, title, rating },
			editBookModal: true
		});
	}

	// Recupération des infos des input
	handleUserInputTitle(e) {
		//on extrait 'newBookData' en dehors du state
		let { newBookData } = this.state;
		//on attribue la valeur du titre recupéré par le input à la propriété 'title'
		newBookData.title = e.target.value;
		//on met à jour le state
		this.setState({
			newBookData
		});
	}
	handleUserInputRating(e) {
		//on extrait 'newBookData' en dehors du state
		let { newBookData } = this.state;

		newBookData.rating = e.target.value;

		this.setState({
			newBookData
		});
	}

	// Edition des infos des input
	editUserInputTitle(e) {
		//on extrait 'newBookData' en dehors du state
		let { editBookData } = this.state;
		//on attribue la valeur du titre recupéré par le input à la propriété 'title'
		editBookData.title = e.target.value;
		//on met à jour le state
		this.setState({
			editBookData
		});
	}
	editUserInputRating(e) {
		//on extrait 'newBookData' en dehors du state
		let { editBookData } = this.state;

		editBookData.rating = e.target.value;

		this.setState({
			editBookData
		});
	}

	deleteBook(id) {
		axios.delete('http://localhost:3000/bookData/' + id).then((response) => {
			this._refreshBooks();
		});
	}

	_refreshBooks() {
		axios.get('http://localhost:3000/bookData').then((response) => {
			this.setState({
				books: response.data
			})
		});
	}

	render() {
		let { books } = this.state;
		let varBooks = books.map((book) => {
			return (
				<tr key={book.id}>
					<td>{book.id}</td>
					<td>{book.title}</td>
					<td>{book.rating}</td>
					<td>
						<Button
							color="success" 
							size="sm" 
							className="mr-2" 
							/* click va envoyer à la fonction editbook() les valeur bindés ci-dessous pour les afficher ds la modal */
							onClick={this.editBook.bind(this, book.id, book.title, book.rating)}>
							Edit
						</Button>
						<Button 
							color="danger"
							size="sm"
							onClick={this.deleteBook.bind(this, book.id)}>
							Delete
						</Button>
					</td>
				</tr>
			)
		});
		// value={this.state.newBookData.title} onChange={(e) => {}}
		return (
			<div className="container">
				<h1>Ma bibliothèque</h1>
				<Button className="my-3" color="danger" onClick={this.toggleNewBookModal}>Ajouter livre</Button>

				<Modal isOpen={this.state.newBookModal} toggle={this.toggleNewBookModal}>
					<ModalHeader toggle={this.toggleNewBookModal}>Add a new book</ModalHeader>
					<ModalBody>
						<FormGroup>
							<Label	for="title">Title</Label>
							<Input	id="title"
											value={this.state.newBookData.title} 
											onChange={this.handleUserInputTitle}
							/>
						</FormGroup>
						<FormGroup>
							<Label for="rating">Rating</Label>
							<Input	id="rating"
											value={this.state.newBookData.rating} 
											onChange={this.handleUserInputRating}
							/>
						</FormGroup>
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={this.addBook}>Confirmer</Button>{' '}
						<Button color="secondary" onClick={this.toggleNewBookModal}>Annuler</Button>
					</ModalFooter>
				</Modal>
				<Modal isOpen={this.state.editBookModal} toggle={this.toggleEditBookModal}>
					<ModalHeader toggle={this.toggleEditBookModal}>Edit a book</ModalHeader>
					<ModalBody>
						<FormGroup>
							<Label	for="title">Title</Label>
							<Input	id="title"
											value={this.state.editBookData.title} 
											onChange={this.editUserInputTitle}
							/>
						</FormGroup>
						<FormGroup>
							<Label for="rating">Rating</Label>
							<Input	id="rating"
											value={this.state.editBookData.rating} 
											onChange={this.editUserInputRating}
							/>
						</FormGroup>
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={this.updateBook}>Confirmer</Button>{' '}
						<Button color="secondary" onClick={this.toggleEditBookModal}>Annuler</Button>
					</ModalFooter>
				</Modal>
				<Table>
					<thead>
						<tr>
							<th>#</th>
							<th>Title</th>
							<th>Rating</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{varBooks}
					</tbody>
				</Table>
			</div>
		);
	}
}

export default App;
